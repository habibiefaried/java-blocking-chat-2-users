/*****
Created By : Habibie Faried
Date : 18 Juli 2013
Deskripsi : File ini merupakan algoritma socket dasar yang digunakan untuk
membuat program Chat Non-Blocking 2 User
******/

import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.lang.*;

class Algorithm
{
	private Socket socket;

	public Algorithm(Socket socket)
	{
		this.socket = socket;
	}

	public void send(String S)
	{
		//Prosedur ini untuk mengirimkan data berupa string
		try
		{
			DataOutputStream out =  new DataOutputStream(socket.getOutputStream());
		   out.writeUTF(S);		
		}	
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public String recv()
	{
		//Prosedur ini untuk menerima data berupa string, mengembalikan string
		try
		{
			DataInputStream in = new DataInputStream(socket.getInputStream());
			return in.readUTF();
		}
		catch (Exception e)
		{
			System.out.println("Lawan bicara offline, keluar dari program. . .");
			System.exit(0);
		}
		return "Error saat menerima data";
	}
}
