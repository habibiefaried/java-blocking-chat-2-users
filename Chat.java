import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.lang.*;
import javax.swing.JOptionPane;

public class Chat
{
	private static String nama;

	public static void Client()
	{
		try
		{
			nama = JOptionPane.showInputDialog("Masukkan Nama anda");
			String IP = JOptionPane.showInputDialog("Masukkan IP Address Server");
			String Port = JOptionPane.showInputDialog("Masukkan Port Server");
			Socket client = new Socket(IP,Integer.parseInt(Port));
			
			Algorithm A = new Algorithm(client);
			System.out.println("Chat dimulai. .");
			Scanner sc = new Scanner(System.in);

			String text = "";
			while (!text.equals("quit"))
			{
				System.out.print("Input teks : "); text = sc.nextLine();
				A.send(nama+" Says : "+text);
				System.out.println("Menunggu respons. . .");
				System.out.println(A.recv());
			}
			System.exit(0);
		}catch (Exception e){System.out.println(e.getMessage());}
	}

	public static void Server()
	{
		try
		{
			nama = JOptionPane.showInputDialog("Masukkan Nama anda");
			String Port = JOptionPane.showInputDialog("Masukkan Port untuk me-listen");
			ServerSocket serverSocket = new ServerSocket(Integer.parseInt(Port));
			System.out.println("Listening Client. . .");
			Socket server = serverSocket.accept();

			Algorithm A = new Algorithm(server);
			System.out.println("Chat dimulai. .");
			Scanner sc = new Scanner(System.in);

			String text = "";
			while (!text.equals("quit"))
			{
				System.out.println("Menunggu respons. . .");
				System.out.println(A.recv());
				System.out.print("Input teks : "); text = sc.nextLine();
				A.send(nama+" Says : "+text);				
			}
			System.exit(0);
		}catch (Exception e){System.out.println(e.getMessage());}
	}

	public static void main(String args[])
	{
		System.out.println("==========================================");
		System.out.println("Pilihan anda ");
		System.out.println("==========================================");
		System.out.println("1. Server");
		System.out.println("2. Client");
		System.out.println("==========================================");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Masukkan pilihan anda : "); int pil = sc.nextInt();
		if (pil == 1) Server();
		else if (pil == 2) Client();
		else System.out.println("Pilihan Salah");
	}
}
